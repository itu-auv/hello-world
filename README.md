# Hello World!
This group is maintained by members of ITU AUV Team, from various faculties and departments of Istanbul Technical University.

<img src="img/ituauv.jpeg" alt="drawing" width="200"/>

The team is preparing for RoboSub'21 and SAUVC'21(Singapore AUV Challange).

## Find us on Web
For details visit our website

[auv.itu.edu.tr Turkish](https://auv.itu.edu.tr/)

[auv.itu.edu.tr English](https://auv.itu.edu.tr/index-eng.html)

## Follow us on Social Media
Twitter: [@ituauvteam](https://twitter.com/ituauvteam?lang=en)

Instagram: [@ituauvteam](https://www.instagram.com/ituauvteam/?hl=en)


## Contact Us
Hey, we're not unreachable. You can contact us with any of the mails below

[ituauvteam@gmail.com](mailto:ituauvteam@gmail.com)

[auv@itu.edu.tr](mailto:auv@itu.edu.tr)

[senceryazici@gmail.com](mailto:senceryazici@gmail.com)

## Credits
A list of our contributors:
- [Sencer Yazici](mailto:senceryazici@gmail.com) - _Team Lead, Software Team Lead_
- [Enes Demirağ](mailto:ensdmrg@gmail.com) - _Software Team Member, Alumni_
- [İrem İpek](mailto:ipek.19@itu.edu.tr) - _Software Team Member_
- [Zeynep Uyar](mailto:zeynuk4622@gmail.com) - _Software Team Member_
- [Ali Eren Karadağ](mailto:aerenkaradag@gmail.com) - _Software Team Member_
- [Mert Can DUZ](mailto:mertcan7033@gmail.com) - _Software Team Member_

## Special Thanks To
[Vatan Aksoy Tezer](mailto:vatanaksoytezer@gmail.com), who founded `ITU Rov Team` and planted the seeds of love of underwater robots, 
`ITU Auv Team` is here to grow it.

## Curtains Close

Copyright © 2018-2021 ITU Auv Team

All rights reserved.

## Goodbye 
_Cricket Noise_


